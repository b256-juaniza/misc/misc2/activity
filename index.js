// Setup dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes.js");
const productRoutes = require("./routes/productRoutes.js")
const app = express();

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Database connection
mongoose.connect("mongodb+srv://admin:admin1234@b256-dizon.choclmo.mongodb.net/Ecommerce?retryWrites=true&w=majority",
{
    useNewUrlParser:true,
    useUnifiedTopology:true
});

app.use("/user", userRoutes);
app.use("/products", productRoutes);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log(`Were connected to the cloud database`));
app.listen(4000, () => console.log(`API is now online on port 4000`));