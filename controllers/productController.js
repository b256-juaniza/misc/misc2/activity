const Product = require("../models/product.js");

// Add Product

module.exports.addProduct = (product) => {

	let newProduct = new Product({
		name : product.name,
		description : product.description,
		price : product.price
	});
	return newProduct.save().then((result, err) => {

		if(err) {

			return false;

		} else {

			return true;

		};
	});
};

// List of all products
module.exports.Allproducts = async () => {

	return  Product.find({}).then(result => {

		return result;

	});
};

// List of all available products
module.exports.Availableproducts = () => {

	return Product.find({isActive: true}).then(result => {

		return result;

	})
};


// Get specifict product using ID
module.exports.getProduct= (reqParams) => {

	return Product.findById(reqParams.Id).then(result => {

		return result;

	})
};

// Update product using product ID
module.exports.updateProduct = (paramsId, prod) => {

	let newProduct = {
		name : prod.name,
		description : prod.description,
		price: prod.price
	};


	return Product.findByIdAndUpdate(paramsId.Id, newProduct).then((result, err) => {
		if (err) {
			return false;
		} else {
			return true;
		}
	})
};

// Archive Product
module.exports.archiveProduct = (paramsId, prod) => {

    return Product.findByIdAndUpdate(paramsId.Id, {isActive : prod.isActive}).then((result, err) => {
        if (err) {
            return false;
        } else {
            return true;
        }
    })
};

